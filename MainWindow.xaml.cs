﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.Runtime.Serialization.Formatters.Binary;

namespace KinectProject
{
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using Microsoft.Kinect;
    using System.Runtime.Serialization;
    using GaitLibrary;
    using System.Threading;

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Ширина выходного чертежа
        /// </summary>
        private const float RenderWidth = 640.0f;

        /// <summary>
        /// Высота выходного чертежа
        /// </summary>
        private const float RenderHeight = 480.0f;

        /// <summary>
        /// Толщина вытянутых соединительных линий
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Толщина эллипса центра тела
        /// </summary>
        private const double BodyCenterThickness = 10;

        /// <summary>
        /// Толщина прямоугольников
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Кисть используется для рисования центральной точки скелета
        /// </summary>
        private readonly Brush centerPointBrush = Brushes.Blue;

        /// <summary>
        /// Кисть используется для рисования суставов, которые в настоящее время отслеживаются
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Кисть используется для рисования суставов, которые в настоящее время выводятся
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Ручка, используемая для рисования костей, которые в настоящее время отслеживаются
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Ручка, используемая для рисования костей, которые в настоящее время выводятся
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Используемый Kinect сенсор
        /// </summary>
        private KinectSensor sensor;

        /// <summary>
        /// Группа чертежа для вывода рендеринга скелета
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Рисование изображения, которое будет отображаться
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Флаг записи
        /// </summary>
        private bool doRecording;

        /// <summary>
        /// Флаг инициализации
        /// </summary>
        private bool doInit;

        /// <summary>
        /// Запись
        /// </summary>
        private GaitRecord record;

        /// <summary>
        /// Количество кадров
        /// </summary>
        private const int MAX_FRAME_COUNT = 100;

        public MainWindow()
        {
            InitializeComponent();
        }

        private static void RenderClippedEdges(Skeleton skeleton, DrawingContext drawingContext)
        {
            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, RenderHeight - ClipBoundsThickness, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, RenderWidth, ClipBoundsThickness));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, RenderHeight));
            }

            if (skeleton.ClippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(RenderWidth - ClipBoundsThickness, 0, ClipBoundsThickness, RenderHeight));
            }
        }

        /// <summary>
        /// Выполнение задач запуска
        /// </summary>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            //Создание основы для походки
            this.record = new GaitRecord(MAX_FRAME_COUNT);

            //Скрытие кнопок
            this.saveButton.IsEnabled = false;
            this.startButton.IsEnabled = false;

            // Создание группы чертежей, которую мы будем использовать для рисования
            this.drawingGroup = new DrawingGroup();

            // Создание источника изображения, который мы можем использовать в нашем управлении изображениями
            this.imageSource = new DrawingImage(this.drawingGroup);

            // Дисплей, использующийся для отображения
            Image.Source = this.imageSource;

            // Просмотреть все датчики и запустить первый подключенный.
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            if (null != this.sensor)
            {
                this.sensor.SkeletonStream.Enable();

                this.sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;

                // Запустить сенсор (Kinect)
                try
                {
                    this.sensor.Start();
                }
                catch (IOException)
                {
                    this.sensor = null;
                }
            }
        }

        /// <summary>
        /// Закрытие окна программы
        /// </summary>
        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (null != this.sensor)
            {
                this.sensor.Stop();
            }
        }

        /// <summary>
        /// Обработчик событий для SkeletonFrameReady
        /// </summary>
        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons = new Skeleton[0];

            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            using (DrawingContext dc = this.drawingGroup.Open())
            {
                dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length != 0)
                {
                    Skeleton skel = GetFirstTrackedSkeleton(skeletons);

                    if (skel != null)
                    {
                        this.DrawBonesAndJoints(skel, dc);
                        //Если инициализация пройдена && идет запись && и кадры больше не добавляются
                        if (GaitRecord.GetInit() && doRecording && record.AddFrame(skel) && GaitRecord.GetFailedFrame() < 15)
                        {
                            StopRecording();
                        }
                        //Если пользователь ждет инициализацию
                        if (doInit)
                        {
                            record.DoInitialize(skel);
                            //Если инициализация прошла
                            if (GaitRecord.GetInit())
                            {
                                startButton.IsEnabled = true;
                                doInit = false;
                                initStatus.Content = "Initialization is successful";
                            }
                            else
                            {
                                startButton.IsEnabled = false;
                                initStatus.Content = "No initialization";
                            }
                        }
                    }
                    else
                    {
                        if (skel == null && doRecording)
                        {
                            saveButton.IsEnabled = false;
                            //Сообщение об ошибке при записи
                            Thread myThread = new Thread(ErrorMessage0); //Создаем новый объект потока (Thread)
                            myThread.Start(); //запускаем поток
                            //Обнуляем количество плохих кадров, чтобы сообщение не повторялось
                            GaitRecord.SetFailedFrame();
                        }
                        StopRecording();
                        if (GaitRecord.GetFailedFrame() > 5)
                        {
                            saveButton.IsEnabled = false;
                            //Сообщение об ошибке при записи
                            Thread myThread = new Thread(ErrorMessage1); //Создаем новый объект потока (Thread)
                            myThread.Start(); //запускаем поток
                            //Обнуляем количество плохих кадров, чтобы сообщение не повторялось
                            GaitRecord.SetFailedFrame();
                        }
                    }
                }

                this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, RenderWidth, RenderHeight));
            }
        }

        private void ErrorMessage0()
        {
            MessageBox.Show("Запись прервана. Человек вышел за границы экрана.", "Запись прервана");
        }

        private void ErrorMessage1()
        {
            MessageBox.Show("Инициализация не выполнена. Скелет некорректен.", "Запись прервана");
        }

        private Skeleton GetFirstTrackedSkeleton(Skeleton[] skeletons)
        {
            foreach (Skeleton skel in skeletons)
            {
                if (skel.TrackingState == SkeletonTrackingState.Tracked)
                {
                    return skel;
                }
            }
            return null;
        }

        /// <summary>
        /// Отрисовка точек и суставов
        /// </summary>
        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
            // Тело
            this.DrawBone(skeleton, drawingContext, JointType.Head, JointType.ShoulderCenter);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.ShoulderRight);
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderCenter, JointType.Spine);
            this.DrawBone(skeleton, drawingContext, JointType.Spine, JointType.HipCenter);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipLeft);
            this.DrawBone(skeleton, drawingContext, JointType.HipCenter, JointType.HipRight);

            // Левая рука
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderLeft, JointType.ElbowLeft);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowLeft, JointType.WristLeft);
            this.DrawBone(skeleton, drawingContext, JointType.WristLeft, JointType.HandLeft);

            // Правая рука
            this.DrawBone(skeleton, drawingContext, JointType.ShoulderRight, JointType.ElbowRight);
            this.DrawBone(skeleton, drawingContext, JointType.ElbowRight, JointType.WristRight);
            this.DrawBone(skeleton, drawingContext, JointType.WristRight, JointType.HandRight);

            // Левая нога
            this.DrawBone(skeleton, drawingContext, JointType.HipLeft, JointType.KneeLeft);
            this.DrawBone(skeleton, drawingContext, JointType.KneeLeft, JointType.AnkleLeft);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleLeft, JointType.FootLeft);

            // Правая нога
            this.DrawBone(skeleton, drawingContext, JointType.HipRight, JointType.KneeRight);
            this.DrawBone(skeleton, drawingContext, JointType.KneeRight, JointType.AnkleRight);
            this.DrawBone(skeleton, drawingContext, JointType.AnkleRight, JointType.FootRight);

            // Отрисовка точек
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.TrackingState == JointTrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (joint.TrackingState == JointTrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Отрисовка точек на экране
        /// </summary>
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            // Преобразование глубины
            DepthImagePoint depthPoint = this.sensor.MapSkeletonPointToDepth(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        /// <summary>
        /// Отрисовка суставов (ребер)
        /// </summary>
        private void DrawBone(Skeleton skeleton, DrawingContext drawingContext, JointType jointType0, JointType jointType1)
        {
            Joint joint0 = skeleton.Joints[jointType0];
            Joint joint1 = skeleton.Joints[jointType1];

            // Если нельзя найти суставы
            if (joint0.TrackingState == JointTrackingState.NotTracked ||
                joint1.TrackingState == JointTrackingState.NotTracked)
            {
                return;
            }

            // Если точки не видны
            if (joint0.TrackingState == JointTrackingState.Inferred &&
                joint1.TrackingState == JointTrackingState.Inferred)
            {
                return;
            }

            // Если обе точки отслеживаются
            Pen drawPen = this.inferredBonePen;
            if (joint0.TrackingState == JointTrackingState.Tracked && joint1.TrackingState == JointTrackingState.Tracked)
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, this.SkeletonPointToScreen(joint0.Position), this.SkeletonPointToScreen(joint1.Position));
        }

        /// <summary>
        /// Начать режим записи
        /// <summary>
        private void StartRecording(object sender, RoutedEventArgs e)
        {
            record = new GaitRecord(MAX_FRAME_COUNT);
            doRecording = true;
            
            startButton.IsEnabled = false;
            saveButton.IsEnabled = false;
        }

        /// <summary>
        /// Закончить режим записи
        /// <summary>
        private void StopRecording()
        {
            doRecording = false;

            if (GaitRecord.GetInit())
            {
                startButton.IsEnabled = true;
            }
            if (record.GetGait().GetJoints().Count == 100)
            {
                saveButton.IsEnabled = true;
            }
        }

        /// <summary>
        /// Сохранить запись
        /// <summary>
        private void SaveRecord(object sender, RoutedEventArgs e)
        {
            saveButton.IsEnabled = false;

            SetTypeGait();

            BinaryFormatter formatter = new BinaryFormatter();

            String path = "";
            if (record.GetGait().GetType() == 0)
            {
                path += "A";
            }
            path += "KinectGait" + System.DateTime.Now.ToString() + ".g";
            path = path.Replace(" ","_"); 
            path = path.Replace(":",".");
            
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, record.GetGait());

                Console.WriteLine("Объект сериализован");
            }

            saveButton.IsEnabled = true;
        }

        /// <summary>
        /// Нажатие на CheckBox о типе походки
        /// <summary>
        private void CheckedTypeGait(object sender, RoutedEventArgs e)
        {
            SetTypeGait();
        }

        /// <summary>
        /// Установить тип походки
        /// <summary>
        private void SetTypeGait()
        {
            if (typeGait.IsChecked == true)
            {
                record.GetGait().SetType(1);
            }
            else
            {
                record.GetGait().SetType(0);
            }
        }

        private void InitSkeleton(object sender, RoutedEventArgs e)
        {
            this.doInit = true;
        }
    }
}
