﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    [Serializable]
    class Gait
    {
        /// <summary>
        /// Возбужденное/невозбужденное состоение при походке
        /// 0 - обычное, 1 - возбужденное отрицательное, 2 - возбужденное положительное
        /// </summary>
        private int type {get; set;}

        /// <summary>
        /// Массив точек для записи
        /// </summary>
        private List<List<double>> joints { get; set; }

        public Gait()
        {
            type = 0;
            joints = new List<List<double>>();
        }

        /// <summary>
        /// Получить массив точек
        /// </summary>
        /// <returns></returns>
        public List<List<double>> GetJoints()
        {
            return this.joints;
        }

        public void SetJoints(List<List<double>> joints)
        {
            this.joints = joints;
        }

        public int GetType()
        {
            return this.type;
        }

        /// <summary>
        /// Задать тип походки (нормальный/аномальный)
        /// </summary>
        /// <param name="abnormal"></param>
        public void SetType(int type)
        {
            this.type = type;
        }
    }
