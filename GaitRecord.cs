﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectProject
{
    using GaitLibrary;
    using Microsoft.Kinect;

    class GaitRecord
    {
        /// <summary>
        /// Количество записанных кадров
        /// </summary>
        private int frameCount;

        /// <summary>
        /// Максимальное количество записанных кадров
        /// </summary>
        private int MaxFrameCount;

        /// <summary>
        /// Информация о походке
        /// </summary>
        private Gait gait;

        /// <summary>
        /// Флаг инициализации
        /// </summary>
        private static bool doInit = false;

        private static List<double> initVectorsLength;

        private static int failedFrame = 0;

        /// <summary>
        /// Конструктор записи походки
        /// <param name="maxFrame" - максимальное число кадров
        /// </summary>
        public GaitRecord(int maxFrame)
        {
            this.gait = new Gait();

            this.frameCount = 0;
            this.MaxFrameCount = maxFrame;
        }

        /// <summary>
        /// Добавление кадра в запись
        /// <param name="skeleton" - скелет человека
        /// <returns - true, если запись полная, false, если место для записи еще есть
        /// </summary>
        public bool AddFrame(Skeleton skeleton)
        {
            List<double> currentSkeletonsJoints = SkeletonToList(skeleton);
            List<double> curVectorsLength = GetVectorsLength(skeleton);

            for (int i = 0; i < initVectorsLength.Count; i++)
            {
                if (Math.Abs((double)(initVectorsLength[i] - curVectorsLength[i]))>0.07)
                {
                    GaitRecord.failedFrame++;
                    return false;
                }
            }

            GaitRecord.failedFrame = 0;

            this.gait.GetJoints().Add(currentSkeletonsJoints);
            this.frameCount++;

            if (this.frameCount == this.MaxFrameCount)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Инициализация скелета
        /// <param name="skeleton" - скелет человека
        /// <return - успешность инициализации>
        /// </summary>
        public bool DoInitialize(Skeleton skeleton)
        {
            foreach(Joint j in skeleton.Joints)
            {
                //Если хотя бы один сустав не отслеживается...
                if (j.TrackingState != JointTrackingState.Tracked)
                {
                    //Прервать инициализацию
                    GaitRecord.doInit = false;
                    return false;
                }
            }
            //Инициализировать новый массив длин векторов
            GaitRecord.initVectorsLength = GetVectorsLength(skeleton);
            //Завершить инициализацию
            GaitRecord.doInit = true;
            return true;
        }

        /// <summary>
        /// Перевод скелета в лист
        /// <param name="skeleton" - скелет человека
        /// <returns - лист с координатами
        /// </summary>
        public List<double> SkeletonToList(Skeleton skeleton)
        {
            List<double> currentSkeletonsJoints = new List<double>();

            foreach (Joint joint in skeleton.Joints)
            {
                if (joint.JointType != JointType.HandLeft
                    && joint.JointType != JointType.HandRight
                    && joint.JointType != JointType.FootLeft
                    && joint.JointType != JointType.FootRight
                    && joint.JointType != JointType.Spine
                    && joint.JointType != JointType.ShoulderCenter)
                {
                    currentSkeletonsJoints.Add(joint.Position.X);
                    currentSkeletonsJoints.Add(joint.Position.Y);
                    currentSkeletonsJoints.Add(joint.Position.Z);
                }
            }

            return currentSkeletonsJoints;
        }

        /// <summary>
        /// Получение длины векторов
        /// <param name="skeleton" - скелет человека
        /// <returns - лист с длинами
        /// </summary>
        public List<double> GetVectorsLength(Skeleton skeleton)
        {
            List<double> lengths = new List<double>();

            //ЛадоньЛ-ЗапястьеЛ
            //lengths.Add(GetVectorLength(skeleton,JointType.HandLeft,JointType.WristLeft));
            //ЗапястьеЛ-ЛокотьЛ
            lengths.Add(GetVectorLength(skeleton, JointType.WristLeft, JointType.ElbowLeft));
            //ЛокотьЛ-ПлечоЛ
            lengths.Add(GetVectorLength(skeleton, JointType.ElbowLeft, JointType.ShoulderLeft));
            //ПлечоЛ-Грудь
            lengths.Add(GetVectorLength(skeleton, JointType.ShoulderLeft, JointType.ShoulderCenter));

            //ЛадоньП-ЗапястьеП
            //lengths.Add(GetVectorLength(skeleton, JointType.HandRight, JointType.WristRight));
            //ЗапястьеП-ЛокотьП
            lengths.Add(GetVectorLength(skeleton, JointType.WristRight, JointType.ElbowRight));
            //ЛокотьП-ПлечоП
            lengths.Add(GetVectorLength(skeleton, JointType.ElbowRight, JointType.ShoulderRight));
            //ПлечоП-Грудь
            lengths.Add(GetVectorLength(skeleton, JointType.ShoulderRight, JointType.ShoulderCenter));

            //СтупняЛ-ЩиколоткаЛ
            //lengths.Add(GetVectorLength(skeleton, JointType.FootLeft, JointType.AnkleLeft));
            //ЩиколоткаЛ-КоленоЛ
            lengths.Add(GetVectorLength(skeleton, JointType.AnkleLeft, JointType.KneeLeft));
            //КоленоЛ-БедроЛ
            lengths.Add(GetVectorLength(skeleton, JointType.KneeLeft, JointType.HipLeft));
            //БедроЛ-Таз
            lengths.Add(GetVectorLength(skeleton, JointType.HipLeft, JointType.HipCenter));

            //СтупняП-ЩиколоткаП
            //lengths.Add(GetVectorLength(skeleton, JointType.FootRight, JointType.AnkleRight));
            //ЩиколоткаП-КоленоП
            lengths.Add(GetVectorLength(skeleton, JointType.AnkleRight, JointType.KneeRight));
            //КоленоП-БедроП
            lengths.Add(GetVectorLength(skeleton, JointType.KneeRight, JointType.HipRight));
            //БедроП-Таз
            lengths.Add(GetVectorLength(skeleton, JointType.HipRight, JointType.HipCenter));

            //Таз-Спина
            lengths.Add(GetVectorLength(skeleton, JointType.HipCenter, JointType.Spine));
            //Спина-Грудь
            lengths.Add(GetVectorLength(skeleton, JointType.Spine, JointType.ShoulderCenter));
            //Грудь-Голова
            lengths.Add(GetVectorLength(skeleton, JointType.ShoulderCenter, JointType.Head));

            return lengths;
        }

        private double GetVectorLength(Skeleton skeleton, JointType joint1, JointType joint2)
        {
            double length, x1, x2, y1, y2, z1, z2;
            int j1 = jointTypeToInt(joint1);
            int j2 = jointTypeToInt(joint2);

            x1 = skeleton.Joints.ElementAt(j1).Position.X;
            y1 = skeleton.Joints.ElementAt(j1).Position.Y;
            z1 = skeleton.Joints.ElementAt(j1).Position.Z;

            x2 = skeleton.Joints.ElementAt(j2).Position.X;
            y2 = skeleton.Joints.ElementAt(j2).Position.Y;
            z2 = skeleton.Joints.ElementAt(j2).Position.Z;

            x1 = x2 - x1;
            y1 = y2 - y1;
            z1 = z2 - z1;

            length = Math.Abs(Math.Sqrt(Math.Pow(x1, 2) + Math.Pow(y1, 2) + Math.Pow(z1, 2)));

            return length;
        }

        private int jointTypeToInt(JointType jointType)
        {
            switch (jointType)
            {
                case JointType.HipCenter:
                    return 0;
                case JointType.Spine:
                    return 1;
                case JointType.ShoulderCenter:
                    return 2;
                case JointType.Head:
                    return 3;
                case JointType.ShoulderLeft:
                    return 4;
                case JointType.ElbowLeft:
                    return 5;
                case JointType.WristLeft:
                    return 6;
                case JointType.HandLeft:
                    return 7;
                case JointType.ShoulderRight:
                    return 8;
                case JointType.ElbowRight:
                    return 9;
                case JointType.WristRight:
                    return 10;
                case JointType.HandRight:
                    return 11;
                case JointType.HipLeft:
                    return 12;
                case JointType.KneeLeft:
                    return 13;
                case JointType.AnkleLeft:
                    return 14;
                case JointType.FootLeft:
                    return 15;
                case JointType.HipRight:
                    return 16;
                case JointType.KneeRight:
                    return 17;
                case JointType.AnkleRight:
                    return 18;
                case JointType.FootRight:
                    return 19;
                default:
                    return -1;
            }

        }

        /// <summary>
        /// Получение походки
        /// <returns - походка
        /// </summary>
        public Gait GetGait()
        {
            return this.gait;
        }

        /// <summary>
        /// Прошла ли инициализация
        /// <returns - true, если прошла, false, если не прошла
        /// </summary>
        public static bool GetInit()
        {
            return GaitRecord.doInit;
        }

        public static int GetFailedFrame()
        {
            return GaitRecord.failedFrame;
        }

        public static void SetFailedFrame()
        {
            GaitRecord.failedFrame = 0;
        }
    }
}
